#include <stdio.h>
#include <unistd.h>
#include <libnet.h>
#include <pcap.h>

#define VERSION "v0.1.2"
#define MAX_PACKET_LEN 1024

int main(int argc, char *argv[])
{
    int opt;
    int verbose = 0;
    char *in_device = NULL;
    char *out_device = NULL;
    char *filter_in = NULL;
    char *in_label = NULL;
    char *out_label = NULL;
    int num_label = 1;

    int len;
    int cur_label;
    
    unsigned label;
    unsigned in_label_int;
    unsigned out_label_int;
    u_char local_packet[MAX_PACKET_LEN];
    
    pcap_t *pcap_handle;
    char pcap_errbuf[PCAP_ERRBUF_SIZE];
    const u_char *pcap_packet;
    struct pcap_pkthdr *pcap_header;
    struct bpf_program pcap_filter;
    
    libnet_t *libnet_handle;
	char libnet_errbuf[LIBNET_ERRBUF_SIZE];
    
    printf("mpls_redirect version %s\tby Daniel Mende - dmende@ernw.de\n", VERSION);
    fflush(stdout);

    while ((opt = getopt(argc, argv, "vhi:o:f:n:I:O:")) != -1) {
        switch (opt) {
        case 'v':
            verbose = 1;
            break;
        case 'i':
            in_device = optarg;
            break;
        case 'o':
            out_device = optarg;
            break;
        case 'f':
            filter_in = optarg;
            break;
        case 'n':
            num_label = atoi(optarg);
            break;
        case 'I':
            in_label = optarg;
            break;
        case 'O':
            out_label = optarg;
            break;
        case 'h':
        default:
            fprintf(stderr, "Usage: %s [-v] -i in_device -o out_device [-f filter] [-n num_label] [-I in_label] -O out_label\n\n", argv[0]);
            fprintf(stderr, "-v\t\t: Be verbose\n");
            fprintf(stderr, "-i in_device\t: Device used for (libpcap-) capture\n");
            fprintf(stderr, "-o out_device\t: Device used for (libnet-) injection\n");
            fprintf(stderr, "-f filter\t: tcpdump filter of matching traffic\n");
            fprintf(stderr, "-n num_label\t: Number of MPLS label to match\n");
            fprintf(stderr, "-I in_label\t: MPLS label to redirect from (DECIMAL)\n");
            fprintf(stderr, "-O out_label\t: MPLS label to redirect to (DECIMAL)\n");
            return 2;
        }
    }
    
    if (!in_device) {
        fprintf(stderr, "No device for capturing given\n");
        return 2;
    }
    if (!out_device) {
        fprintf(stderr, "No device for injection given\n");
        return 2;
    }
    if (!out_label) {
        fprintf(stderr, "No output label given\n");
        return 2;
    }

    if (getuid() != 0) {
        fprintf(stderr, "You must be root for redirecting\n");
        return 2;
    }
    
    pcap_handle = pcap_open_live(in_device, BUFSIZ, 1, 1000, pcap_errbuf);
    if (pcap_handle == NULL) {
        fprintf(stderr, "Couldn't open device: %s\n", pcap_errbuf);
        return 2;
    }
    if (verbose)
        printf("Capturing on device %s\n", in_device);
    if (filter_in) {
        if (pcap_compile(pcap_handle, &pcap_filter, filter_in, 0, 0) == -1) {
            fprintf(stderr, "Couldn't parse filter: %s\n", pcap_geterr(pcap_handle));
            return 2;
        }
        if (pcap_setfilter(pcap_handle, &pcap_filter) == -1) {
            fprintf(stderr, "Couldn't install filter: %s\n", pcap_geterr(pcap_handle));
            return 2;
        }
        if (verbose)
            printf("Using filter %s\n", filter_in);
    }
    
	libnet_handle = libnet_init(LIBNET_LINK_ADV, out_device, libnet_errbuf);
    if (libnet_handle == NULL) {
        fprintf(stderr, "Couldn't open device: %s\n", libnet_errbuf);
        return 2;
    }
    if (verbose)
        printf("Injecting on device %s\n", out_device);

    if (in_label)
        in_label_int = atol(in_label);
    out_label_int = atoi(out_label);
    if (verbose)
        printf("Redirecting to MPLS label %i\n", out_label_int);
    while (pcap_next_ex(pcap_handle, &pcap_header, &pcap_packet) > 0) {
        if (pcap_packet[12] != 0x88 || pcap_packet[13] != 0x47)
            continue;
        fflush(stdout);
        len = pcap_header->len > MAX_PACKET_LEN ? MAX_PACKET_LEN : pcap_header->len;
        memcpy(local_packet, pcap_packet, len);
        for (cur_label = 1; cur_label < num_label && cur_label > 0; ++cur_label)
            if (*((unsigned *) (local_packet + 14 + (cur_label-1) * 4)) & htonl(0x00000100))
                cur_label = 0;
        if (cur_label == 0) {
            if (verbose)
                printf("#");
            continue;
        }
        label = ntohl(*((unsigned *) (local_packet + 14 + (cur_label-1) * 4)) & htonl(0xfffff000)) >> 12;
        if (in_label && label != in_label_int) {
            if (verbose)
                printf(".");
            continue;
        }
        if (verbose)
            printf("*");
        *((unsigned *) (local_packet + 14 + (cur_label-1) * 4)) &= htonl(0x00000fff);
        *((unsigned *) (local_packet + 14 + (cur_label-1) * 4)) |= htonl(out_label_int << 12);
        if (libnet_write_link(libnet_handle, local_packet, len) < 0) {
            fprintf(stderr, "Couldn't write packet: %s\n", libnet_geterror(libnet_handle));
            return 2;
        }
    }

    return 0;
}
